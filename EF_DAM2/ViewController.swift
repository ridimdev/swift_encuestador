//
//  ViewController.swift
//  EF_DAM2
//
//  Created by P on 11/29/19.
//  Copyright © 2019 Cibertec. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var tfraz: UITextField!
    @IBOutlet weak var lbmsj: UILabel!
    
    @IBOutlet weak var tblista: UITableView!
    @IBOutlet weak var tfdir: UITextField!
    @IBOutlet weak var tfru: UITextField!
    var oLista = [Proveedor_BE]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblista.dataSource=self
        self.tblista.delegate=self
        tblista.rowHeight = 118
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return oLista.count
    }
    func numberOfSections(in tableview : UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let ocelda = tableView.dequeueReusableCell(withIdentifier: "celda1", for: indexPath) as! TableViewCellCelda
        let reg = oLista[indexPath.row]
        ocelda.mostrar(pentidad: reg)
        return ocelda
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("actualiza elimina")
        let indexPath = tableView.indexPathForSelectedRow!
        let currentCell = tableView.cellForRow(at: indexPath) as! TableViewCellCelda
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "ViewControllerActualizaElimina") as! ViewControllerActualizaElimina
        print("actualiza elimina0")
        vc.cod = currentCell.lbCodigo.text!
        vc.raz = currentCell.lbRazon.text!
        vc.ruc = currentCell.lbRuc.text!
        vc.dir = currentCell.lbDireccion.text!
        print("actualiza elimina1")
        self.navigationController?.pushViewController(vc, animated: true)
        print("actualiza elimina2")
    }

    @IBAction func btnBuscar(_ sender: Any) {
        oLista = [Proveedor_BE]()
        let urlListado = "http://wscibertec201922.somee.com/Proveedor/Lista?RazonSocial="+tfraz.text!
        print(urlListado)
        let urlConsulta = URL(string: urlListado)
        let peticion = URLRequest(url: urlConsulta!)
        
        let tarea = URLSession.shared.dataTask(with: peticion)
        {Datos, Respuesta, Error in
            print("por iniciar")
            if(Error == nil)
            {
                print("por procesar datos")
                print(Datos ?? "Vacio")
                let datosCadena = NSString(data: Datos!,encoding: String.Encoding.utf8.rawValue)
                print(datosCadena!)
                print("fin procesar datos")
                
                DispatchQueue.main.async {
                    let JSON = try? JSONSerialization.jsonObject(with: Datos!, options: [])
                    
                    if let dictionary = JSON as? [String: Any], let wslista = dictionary["ObjListaPROVEEDOR"] as? [[String: Any]]
                    {
                        wslista.forEach{ Registro in
                            let CodigoProveedor = Registro["CodigoProveedor"] as! Int
                            let RazonSocial = Registro["RazonSocial"] as? String
                            let Direccion = Registro["Direccion"] as? String
                            let Ruc = Registro["Ruc"] as? String
                            
                            let proveedor1 = Proveedor_BE(prazonsocial: RazonSocial ?? "", pdireccion: Direccion ?? "", pruc: Ruc ?? "")
                            proveedor1.codigoproveedor = CodigoProveedor
                            self.oLista.append(proveedor1)
                        }
                        self.lbmsj.text = "Mensaje: " + String(self.oLista.count) + " usuarios"
                        self.tblista.reloadData()
                    }
                }
            }
            else
            {
                print("Error")
                print(Error ?? "Error vacio")
            }
        }
        tarea.resume()
    }
}

