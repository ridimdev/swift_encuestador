//
//  ViewControllerActualizaElimina.swift
//  EF_DAM2
//
//  Created by P on 11/29/19.
//  Copyright © 2019 Cibertec. All rights reserved.
//

import UIKit

class ViewControllerActualizaElimina: UIViewController {
    var cod : String = ""
    var raz : String = ""
    var ruc : String = ""
    var dir : String = ""
    @IBOutlet weak var tfcodigo: UITextField!
    @IBOutlet weak var tfrazonsocial: UITextField!
    @IBOutlet weak var tfdireccion: UITextField!
    @IBOutlet weak var tfruc: UITextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tfcodigo.text = cod
        tfrazonsocial.text = raz
        tfdireccion.text = dir
        tfruc.text = ruc
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnActualizar(_ sender: Any) {
        var urlActualizar = "http://wscibertec201922.somee.com/Proveedor/RegistraModifica?pTipoTransaccion=A&CodigoProveedor="+tfcodigo.text!+"&RazonSocial="+tfrazonsocial.text!+"&Direccion="+tfdireccion.text!+"&Ruc="+tfruc.text!
        urlActualizar = urlActualizar.split(separator: " ").joined(separator: "+")
        print(urlActualizar)
        let urlPush = URL(string: urlActualizar)
        let peticion = URLRequest(url: urlPush!)
        
        let tarea = URLSession.shared.dataTask(with: peticion)
        {Datos, Respuesta, Error in
            print("por iniciar")
            if(Error == nil)
            {
                print("por procesar datos")
                print(Datos ?? "Vacio")
                let datosCadena = NSString(data: Datos!,encoding: String.Encoding.utf8.rawValue)
                print(datosCadena!)
                print("fin procesar datos")
            }
            else
            {
                print("Error")
                print(Error ?? "Error vacio")
            }
        }
        tarea.resume()
    }
    
    @IBAction func btnEliminar(_ sender: Any) {
        var urlEliminar = "http://wscibertec201922.somee.com/Proveedor/Elimina?pCodigoProveedor="+tfcodigo.text!
        urlEliminar = urlEliminar.split(separator: " ").joined(separator: "+")
        print(urlEliminar)
        let urlPush = URL(string: urlEliminar)
        let peticion = URLRequest(url: urlPush!)
        
        let tarea = URLSession.shared.dataTask(with: peticion)
        {Datos, Respuesta, Error in
            print("por iniciar")
            if(Error == nil)
            {
                print("por procesar datos")
                print(Datos ?? "Vacio")
                let datosCadena = NSString(data: Datos!,encoding: String.Encoding.utf8.rawValue)
                print(datosCadena!)
                print("fin procesar datos")
            }
            else
            {
                print("Error")
                print(Error ?? "Error vacio")
            }
        }
        tarea.resume()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
