//
//  ViewControllerNuevo.swift
//  EF_DAM2
//
//  Created by P on 11/29/19.
//  Copyright © 2019 Cibertec. All rights reserved.
//

import UIKit

class ViewControllerNuevo: UIViewController {
    @IBOutlet weak var tfcodigo: UITextField!
    @IBOutlet weak var tfrazon: UITextField!
    
    @IBOutlet weak var tfruc: UITextField!
    @IBOutlet weak var tfdireccion: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func btnRegistrar(_ sender: Any) {
        
        var urlGrabar = "http://wscibertec201922.somee.com/Proveedor/RegistraModifica?pTipoTransaccion=N&CodigoProveedor="+tfcodigo.text!+"&RazonSocial="+tfrazon.text!+"&Direccion="+tfdireccion.text!+"&Ruc="+tfruc.text!
        urlGrabar = urlGrabar.split(separator: " ").joined(separator: "+")
        print(urlGrabar)
        let urlPush = URL(string: urlGrabar)
        let peticion = URLRequest(url: urlPush!)
        
        let tarea = URLSession.shared.dataTask(with: peticion)
        {Datos, Respuesta, Error in
            print("por iniciar")
            if(Error == nil)
            {
                print("por procesar datos")
                print(Datos ?? "Vacio")
                let datosCadena = NSString(data: Datos!,encoding: String.Encoding.utf8.rawValue)
                print(datosCadena!)
                print("fin procesar datos")
            }
            else
            {
                print("Error")
                print(Error ?? "Error vacio")
            }
        }
        tarea.resume()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
